<?php

class scanDir { # Credits to  gambit_642 AT hotmailDOTcom az php.net (https://www.php.net/manual/en/function.scandir.php)
    static private $directories, $files, $ext_filter, $recursive;

// ----------------------------------------------------------------------------------------------
    // scan(dirpath::string|array, extensions::string|array, recursive::true|false)
    static public function scan(){
        // Initialize defaults
        self::$recursive = false;
        self::$directories = array();
        self::$files = array();
        self::$ext_filter = false;

        // Check we have minimum parameters
        if(!$args = func_get_args()){
            die("Must provide a path string or array of path strings");
        }
        if(gettype($args[0]) != "string" && gettype($args[0]) != "array"){
            die("Must provide a path string or array of path strings");
        }

        // Check if recursive scan | default action: no sub-directories
        if(isset($args[2]) && $args[2] == true){self::$recursive = true;}

        // Was a filter on file extensions included? | default action: return all file types
        if(isset($args[1])){
            if(gettype($args[1]) == "array"){self::$ext_filter = array_map('strtolower', $args[1]);}
            else
            if(gettype($args[1]) == "string"){self::$ext_filter[] = strtolower($args[1]);}
        }

        // Grab path(s)
        self::verifyPaths($args[0]);
        return self::$files;
    }

    static private function verifyPaths($paths){
        $path_errors = array();
        if(gettype($paths) == "string"){$paths = array($paths);}

        foreach($paths as $path){
            if(is_dir($path)){
                self::$directories[] = $path;
                $dirContents = self::find_contents($path);
            } else {
                $path_errors[] = $path;
            }
        }

        if($path_errors){echo "The following directories do not exists<br />";die(var_dump($path_errors));}
    }

    // This is how we scan directories
    static private function find_contents($dir){
        $result = array();
        $root = scandir($dir);
        foreach($root as $value){
            if($value === '.' || $value === '..') {continue;}
            if(is_file($dir.DIRECTORY_SEPARATOR.$value)){
                if(!self::$ext_filter || in_array(strtolower(pathinfo($dir.DIRECTORY_SEPARATOR.$value, PATHINFO_EXTENSION)), self::$ext_filter)){
                    self::$files[] = $result[] = $dir.DIRECTORY_SEPARATOR.$value;
                }
                continue;
            }
            if(self::$recursive){
                foreach(self::find_contents($dir.DIRECTORY_SEPARATOR.$value) as $value) {
                    self::$files[] = $result[] = $value;
                }
            }
        }
        // Return required for recursive search
        return $result;
    }
}

$dirs = array("adatok", "backup", "csvtoimport");

$files = scanDir::scan($dirs, false, true);
$files = array_unique ($files);

# Itt a beadott linket ketté kell vágni, mert a feltöltésnél teljesen más a curl parancs szintaktikája.
$key = preg_replace ('/(https:\/\/.+\/public.php\/webdav\/)(.+)/', '$2', $argv[1]);
$url = preg_replace('/(https:\/\/.+\/public.php\/webdav\/)(.+)/', '$1', $argv[1]);
# var_dump ($key);
# var_dump ($url);

/* Ezt a fájl és mappalekérdezést, feltöltést alaposabban át kell gondolni. Jelenleg működik, de ha új mappák kerülnek be a rendszerbe,
lehet,hogy gond lesz vele. */

# A mappák tömbjét a fileokból szedjük le úgy, hogy levesszük a fájlnevet (adatok/teszt/tesztadat.sqlite -> adatok/teszt/)
$local_folders = preg_replace('/(.+\/)(.+\..+)/', '$1', $files);
# var_dump ($local_folders);
# Ki kell venni az adatok mappát, mert az biztosan ott van, hiszen azt töltöttük le.
$diff= preg_grep('/([adatok]+)(\/.+)/', $local_folders);
$local_folders = array_diff ($local_folders, $diff);
$local_folders = array_unique ($local_folders);
# var_dump ($local_folders);

# Le kell kérdezni a fenti mappastruktúrát.

# Ki kell egészíteni, ha hiányzik valami. (Most minden mappát megpróbál elkészíteni, ha már létezik, ad egy hibaüzenetet.)
foreach ($local_folders as $local_folder) {
  #echo ("curl -k -X MKCOL -u \"$key:\" -H 'X-Requested-With: XMLHttpRequest' $url$local_folder") . PHP_EOL;
  exec ("curl -k -X MKCOL -u \"$key:\" -H 'X-Requested-With: XMLHttpRequest' $url$local_folder");
}


# Fel kell tölteni a fájlokat.

foreach ($files as $file) {
  exec ("curl -k -T $file -u \"$key:\" -H 'X-Requested-With: XMLHttpRequest' $url\/$file");
}

?>
