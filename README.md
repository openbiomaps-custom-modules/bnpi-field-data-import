# BNPI terepi adat importálás a Dél-hevesi Tájegység számára

## Rövid leírás

OpenBioMaps egyedi modul felhőben tárolt, QField terepi projektekben született adatok importálására

A modul publikus hivatkozás segítségével letölti az ""/adatok" könyvtár tartalmát, készít egy biztonsági mentést, majd az egyes sqlite fájlokban található pontrétegek közül a felhasználó által választottakat csv-be konvertálja, és a megfelelő OBM projekttáblához adja.

A sikeres import után az importált rétegeket kiüríti, és a módosított sqlite állományokat, a biztonsági mentéseket és a csv fájlokat visszatölti a felhőbe.

## Követelmények

Linux futtatókörnyezet, minimálisan az alábbi telepített csomagokkal:

- php
- gdal-bin (ogr2ogr, ogrinfo, gdalsrsinfo)
- unzip
- curl

A cél, hogy a modul minél általánosabban használható legyen, azonban jelenleg feltételez egy viszonylag kötött fájlstruktúrát, jelesül, hogy egy adatok mappának almappáiban találhatók az egyes sqlite állományok (a BNPI egyes tájegységeinek terepi adatrögzítéséhez használt struktúra). Később ez is fejleszthető.

## Szintaxis

A parancssorban az alábbi parancsot kell kiadni:

php dhte_field_data_import.php publikus-link

A publikus link a felhőszolgáltatóra bejelentkezve, a megfelelő mappa publikus megosztásával hozható létre.

Jelenleg csak a BNPI felhőszervere támogatott.

Tesztelésre elérhetők adatok az alábbi linken: https://owncloud.bnpi.hu/index.php/s/BZsZEBdXu4jmyZb (3,5 MB)

## Jelenleg elkészült funkciók

- Alapvető ellenőrzések (operációs rendszer, parancsok, link validálás).
- Letöltés, kicsomagolás.
- Rétegnevek felsorolása.
- Pontrétegek leválogatása
- Adatbázis állományok biztonsági mentése.
- Felhasználó által rétegválasztás.
- Adatbázis állományok leválogatása az alapján, hogy tartalmazzák-e az adott réteget.
- Az egyes adatbázisokban található egymásnak megfelelő rétegek csv-be konvertálása és egybefűzése.
- Az importálás után a csv fájl áthelyezése.
- Az sqlite réteg kiürítése importálást követően, ha biztonsági másolatként létrejött csv is a megfelelő helyen van.
- Egy-egy réteg feltöltése után vissza lehet térni újabb réteg feltöltésére, ha van még feltölthető réteg, és a felhasználó is szeretné.
- A feltöltések végén a kiürített datbázis fájlokat és a mentéseket visszatölti az eredeti mappába.

## Ismert hibák, hiányosságok (aka Kiadási megjegyzések), nem fontossági sorrendben, hanem ahogy előjött, vagy eszembe jutott.

1. A spatialite állományok tartalmazhatnak olyan mezőtípusokat, amelyeket a csv nem támogat (pl. binary), ezért az ilyen rétegek importálása nem ajánlott.
2. A 'null' értéket az ogr2ogr bizonyos esetekben '0' értékre cseréli, ami gondot jelenthet.
3. Ha a felhasználó melléüt, és nem jó réteget választ először, a 68. sorban a $selected_layer változó már nem tömb. Ezt ki kell deríteni, miért van?
4. Az ImportFirstStep funkciót le kell egyszerűsíteni, mert nagyon béna.
5. Kilépés helyett menet közben is fel kell ajánlani a feltöltés opciót, mert nem minden esetben akarjuk majd az összes réteget exportálni.
(6. Hiányzik még a felhőbe történő visszatöltés. [Megoldva.])
7. Magát az OBM feltöltést még nem hívja meg, valós OBM import nem történik.
8. Nincs kialakítva böngészős környezet, jelenleg parancssorban fut.
9. OBM modult kell belőle faragni.
10. Ki kell szűrni a 0 elemet tartalmazó rétegeket is az elején.
(11. Az adatbázisok egyes rétegeinek kiürítésekor megpróbálja azokat is üríteni, amikben nem található meg az adott réteg. [Megoldva.])
12. Jó lenne az ogr2ogr warning üzenetekkel valamit kezdeni.
13. Mappakezelést át kell majd alakítani.
14. Az array_push-t le kell cserélni $array[] = "kifejezés";-re.
15. Ki kell törölni minden fájlt a munkakönyvtárból a végén, ha sikeresek voltak a feltöltések.
16. Az importált csv-ket is be kell zippelni.
