<?php

/*Attila Ferenc, 2019-2020. Some credits: OpenBioMaps team, php.net*/

/*Be kell állítani az osztályt, stb. stb.*/

// check system command exists
function command_exist($cmd) {
  //$returnVal = shell_exec(sprintf("which %s", escapeshellarg($cmd)));
  $returnVal = exec(sprintf("which %s", escapeshellarg($cmd)));
  return !empty($returnVal);
}

function ImportFirstStep ($layers) {
  echo PHP_EOL . PHP_EOL . "Az alábbi rétegeket lehet importálni:" . PHP_EOL;
  foreach ($layers as $layer) {
    print "$layer" . PHP_EOL;
  }
  echo PHP_EOL . "Szeretné folytatni? Ha igen, írja be, hogy 'i'. Minden egyéb esetben kilépek: ";
  $handle = fopen ("php://stdin","r");
  $line = fgets ($handle);
  if (trim ($line) != 'i'){
      echo PHP_EOL . "Kilépés!" . PHP_EOL;
      exit;
  }
  echo PHP_EOL . "Köszönöm, folytathatjuk..." . PHP_EOL;
}


function LayerSelection ($layers) { /*Ezt itt majd ki kell találni, hogy pontosan hogy helyes. Most működik.*/
  echo PHP_EOL . "Válasszon az alábbi listából egy réteget, majd adja meg a réteg sorszámát!" . PHP_EOL;
    foreach ($layers as $layer_key=>$layer_value) {
      print "$layer_key: $layer_value" . PHP_EOL . PHP_EOL;
    }
    echo "Adja meg egy réteg sorszámát: "; # Réteg sorszámnak meg kell adni a réteg tömb kulcsát.
    echo PHP_EOL;
    $handle = fopen ("php://stdin","r");
    $line = fgets ($handle);
    if (!array_key_exists (trim ($line), $layers)) {
        echo PHP_EOL . "Nem létező réteg!" . PHP_EOL; # Ha a kulcs nem létezik, akkor vagy új kuklcsérték megadása, vagy kilépés.
        unset ($line);
        unset ($handle);
        echo PHP_EOL . "Adjon meg új sorszámot, vagy írja be az 'exit' parancsot a kilépéshez: ";
        $handle = fopen ("php://stdin","r");
        $line = fgets ($handle);
        if (trim ($line) === 'exit'){
          echo PHP_EOL . "Kilépés!" . PHP_EOL;
          exit;
        }
        elseif (!array_key_exists (trim ($line), $layers)) {
          echo PHP_EOL . "Nem létező réteg! Kezdjük újra!" . PHP_EOL;
          LayerSelection ($layers); # Ha másodszorra sem jó az érték, újra kiíratjuk a rétegeket a kulcsaikkal együtt (Vissza az elejére).
        }
    }
    else {
      # A felhasználó által beírt értéket berakjuk egy tömb kulcsának és értékének (ez utóbbi nem feltétlenül szükséges, de kell value (?) a tömbbe).
      $selected_layer = array ();
      $selected_layer = array_fill_keys (array (trim ($line)), trim($line));
      return $selected_layer = array_intersect_key ($layers, $selected_layer); # Megkapjuk a beírt kulcshoz tartozó réteget.
    }
}

function Import ($selected_layer, $databases) {
  /*Mi van azokkal az adatbázisokkal, amikben az adott réteg nem szerepel? Itt ki kell venni őket.*/

  # Ehhez újra lekérdezzük az adatbázisokban lévő rétegeket, de most fájlonként megyünk végig rajtuk,
  # és mindegyiknél előállítjuk a rétegneveket ($database_layers).
  $selected_layer = implode($selected_layer); # Át kell alakítani stringre, hogy az exec tudja kezelni.
    foreach ($databases as $database){
  	   $info = (exec ("ogrinfo \"$database\"", $database_layers)); # Hát ez nem túl elegáns, szebben kellene.
       # var_dump ($database_layers);
       $database_layers = preg_grep('/(\A[0-9])/', $database_layers);
       $database_layers = preg_grep('/([0-9]{1,5})(:\s)(.+)(\s)(\(M?u?l?t?i?)(\s?)(Point)/', $database_layers);
       $database_layers = preg_replace('/([0-9]{1,5})(:\s)(.+)(\s)(\(.+)/', '$3', $database_layers);
       if (!in_array ($selected_layer, $database_layers)) { # Kiszűrjük és eltávolítjuk az(oka)t az állományokat, amikben nem szerepel az adott néven pontfedvény. !!!Nagybetűérzékeny!!!
         $databases = array_diff($databases, array($database));
         print "A(z) $database állományt elvetettem, mert nem szerepel benne a kiválasztott réteg.";

       }
       unset ($database_layers);
     }
  # var_dump ($databases);

  # Convert selected layer to csv and merge it from all the databases

  chdir ("../");
  $folders=glob("csvtoimport");
  if (empty($folders)){
  	mkdir("csvtoimport");
  }

  # Ha már létezik a csv fájl, meg kell kérdezni, a felhasználót, hogy felülírja a csv-t, hozzáfűzi az új elemeket, más réteget választ,
  # vagy inkább kilép a programból.

  foreach ($databases as $database){
    # A kiválasztott réteget átfordítja csv-be, és összefűzi az egyes adatbázis fájlokból származó azonos rétegeket.
    system ("ogr2ogr -update -append -addfields -f \"CSV\" csvtoimport/$selected_layer.csv adatok/$database $selected_layer -dsco GEOMETRY=AS_WKT");
  }

  /* A null -> 0 probléma kérdéskörére kell választ keresni:
  Warning 1: Value 'NULL' of field RTM.fiokaszam parsed incompletely to integer 0.
  fieldTypeToString és mapFieldType nem jó*/

  /*Kell-e ellenőrizni, hogy a fájl létrejött? Egyelőre nincs ilyen funkció, szerintem nem is kell.*/

  /* Az elkészült csv fájlokat át kellene adnia a feltöltő felületnek, */

  /*... majd sikeres import után egy "imported" mappába áthelyezni.*/

  chdir ("backup");
  $folders=glob("imported_csv");
  if (empty($folders)){
  	mkdir("imported_csv");
  }
  chdir ("../");

  system ("mv csvtoimport/$selected_layer.csv backup/imported_csv/$selected_layer.csv");

  return $selected_layer;
}

function EmptyLayer ($selected_layer, $databases) {
  # Egy ellenőrzés, hogy a csv fájl létrejött-e? Később úgyis kell ez a tömb.

  $importedcsv = (scandir ("backup/imported_csv"));
  $importedcsv = array_diff ($importedcsv, array (".", ".."));
  $importedcsv = preg_replace('/(.+)(\.csv)/i', '$1', $importedcsv);
  if (!in_array ($selected_layer, $importedcsv)) {
    echo PHP_EOL . PHP_EOL . "A csv biztonsági másolat nem jött létre, ezért az adatbázis rétegeit biztonsági megfontolásokból nem ürítem." . PHP_EOL;
    echo "Kilépek!" . PHP_EOL . PHP_EOL;
  }

# Keressük azokat a fájlokat, amikben megtalálható az éppen importált réteg. Ehhez újra le kell futtatni a LayerSelection funkció egy részét:
  chdir ("adatok");
  # var_dump ($databases);
  foreach ($databases as $database){
    $info = (exec ("ogrinfo \"$database\"", $database_layers)); # Hát ez nem túl elegáns, szebben kellene.
    var_dump ($database_layers);
    $database_layers = preg_grep('/(\A[0-9])/', $database_layers);
    $database_layers = preg_grep('/([0-9]{1,5})(:\s)(.+)(\s)(\(M?u?l?t?i?)(\s?)(Point)/', $database_layers);
    $database_layers = preg_replace('/([0-9]{1,5})(:\s)(.+)(\s)(\(.+)/', '$3', $database_layers);
    if (!in_array ($selected_layer, $database_layers)) { # Kiszűrjük és eltávolítjuk az(oka)t az állományokat, amikben nem szerepel az adott néven pontfedvény. !!!Nagybetűérzékeny!!!
      $databases = array_diff($databases, array($database));
    }
    unset ($database_layers);
  }
  # var_dump ($databases);

  # Empty exported layer

  foreach ($databases as $database){
  	system ("ogrinfo $database -sql \"DELETE FROM $selected_layer\"");
  	system ("ogrinfo $database -sql \"VACUUM\"");
  }
  return $importedcsv;

}

# Check operating system
if (strtoupper(PHP_OS) === 'LINUX') {
    echo PHP_EOL . 'OK! A program Linux környezetben fut.' . PHP_EOL . PHP_EOL;
} else {
    echo PHP_EOL . 'HIBA! Nem Linux operációs rendszert találtam, így nem lehet garantálni a sikeres végrehajtást!' . PHP_EOL . PHP_EOL;
    die;
}

if (!command_exist('ogrinfo')) {
	echo ('HIBA! Az ogrinfo parancs nem található! Valószínűleg telepíteni kell a gdal-bin csomagot!');
  return false;
}

if (!command_exist('ogr2ogr')) {
		echo ('HIBA! Az ogr2ogr parancs nem található! Valószínűleg telepíteni kell az gdal-bin csomagot!');
		return false;
}

if (!command_exist('unzip')) {
		echo ('HIBA! Az unzip parancs nem található! Valószínűleg telepíteni kell az unzip csomagot');
		return false;
}

if (!command_exist('curl')) {
		echo ('HIBA! A curl parancs nem található! Valószínűleg telepíteni kell a curl csomagot');
		return false;
}

# Download sqlite files from cloud (with the public link of "adatok" folder), and then unzip it.

if (!isset ($argv[1])) {
  $argv[1] = null;
}
$input = array('0' => $argv[1]);
$input = preg_grep ('/(https:\/\/owncloud\.bnpi\.hu\/index.php\/[s]\/[a-zA-Z0-9]+)/', $input); /*A beírt link helyességét ellenőrzi*/

if (!empty ($input)) {

  /*Letölti a felhőből a (path/to/folder/adatok/) tartalmát egy zip fájlként*/
	exec ("curl --output \"download.zip\" -O $argv[1]\/download");
  /*kicsomagolja, és ha kell frissíti a szükséges állományokat, kiszűri a master adatbázist, aminek üresnek kell lennie.*/
  exec ("unzip -o download.zip adatok/*/*.sqlite -x adatok/master/*");
}

else {
  echo (file_get_contents ("README.md"));
	print PHP_EOL . "HIBA! Érvénytelen publikus hivatkozás!" . PHP_EOL; /*Ha nem felel meg a link a feltételeknek, akkor kilép*/
	die;
}

# Get spatialite files from "adatok/initial/" folder.

$datadirs = scandir ("adatok"); /*Begyűjti az adatok mappában lévő mappaneveket*/
$datadirs = array_diff($datadirs, array (".", "..")); /*Kiveszi a . és .. értékeket a tömbből*/
$databases = array ();
chdir ("adatok");

foreach ($datadirs as $datadir){
	array_push ($databases, implode(glob("$datadir/*.sqlite"))); /*Az összes sqlite fájlt beteszi a $databases tömbbe.*/
}

# Backup database files

# Vissza kell menni a programkönyvtárba, ott jön majd létre a backup.
chdir ("../");
$folders=glob("backup");
if (empty($folders)){
	mkdir("backup");
}
$backuptime=date("Y_m_d_H_i_s");
fopen("backup/adatmentes_$backuptime.zip", "w");
$zip=new ZipArchive;
$zipfile="backup/adatmentes_$backuptime.zip";
if ($zip->open($zipfile, ZipArchive::CREATE)!==TRUE) {
exit(PHP_EOL . "Nem tudom létrehozni a <$zipfile> archivált állományt!" . PHP_EOL);
}

foreach ($databases as $database){
	$zip->open($zipfile);
	$zip->addFile("adatok/$database");
	$zip->close();
}

# Irány dolgozni az adatok könyvtárba!
chdir ("adatok");

/*Itt le kell kérdezni az összes sqlite pontfedvényt, olyan tömböt előállítani, ahol minden
rétegnév csak egyszer szerepel, és ezekből válasszon a felhasználó.*/

# Az sqlite információk beszerzése
$info = array();
$layers = array();
foreach ($databases as $database){
	$info = (exec ("ogrinfo \"$database\"", $layers));
}

/* Tényleges rétegek kiszűrése, egyéb infók eldobása: INFO: Open of `adatok/FA/adatgyujtes_FA.sqlite'
      using driver `SQLite' successful.*/
$layers = preg_grep('/(\A[0-9])/', $layers);
# 2: kultura_ID (Multi Point)

# Pontrétegek leválogatása
$layers = preg_grep('/([0-9]{1,5})(:\s)(.+)(\s)(\(M?u?l?t?i?)(\s?)(Point)/', $layers);
# Ha nincsenek pontfedvények, a program kilép.
if (empty ($layers)) {
  echo "Nincs importálható pontfedvény az adatbázis állomány(ok)ban, ezért kilépek!" . PHP_EOL . PHP_EOL;
  die;
}

/*Ide kell egy ellenőrzés, hogy nem-e üres a tömb. Ha igen ezt közölni kell a felhasználóval, és ki kell lépni.*/

# Rétegnevek levágása
$layers = preg_replace('/([0-9]{1,5})(:\s)(.+)(\s)(\(.+)/', '$3', $layers);

# Egyedi értékek megtartása, így minden adatbázis egyedi rétegelnevezései felsorolásra kerülnek.
$layers = array_unique($layers);
# var_dump ($layers);

ImportFirstStep ($layers);

$selected_layer = LayerSelection ($layers);

$selected_layer = Import ($selected_layer, $databases);

$importedcsv = EmptyLayer ($selected_layer, $databases);

# Kérdés újabb réteg feltöltésére
/* Itt majd ki kell szűrni a már importált rétegeket, és egy ellenőrzést lefuttatni, hogy van-e még importálható réteg!!! */

$layers = array_diff ($layers, $importedcsv);

while (!empty($layers)) {
  ImportFirstStep ($layers);
  $selected_layer = LayerSelection ($layers);
  $selected_layer = Import ($selected_layer, $databases);
  $importedcsv = EmptyLayer ($selected_layer, $databases);
  $layers = array_diff ($layers, $importedcsv);
}

echo PHP_EOL . PHP_EOL . "Minden pontfedvény importálásra került. Visszatölthetem a változásokat az eredeti tárhelyre?" . PHP_EOL . PHP_EOL;
/*A kiürített sqlite állományokat, a backupot az imported és a csv mappákat (ha nem üresek) vissza kell tölteni a felhőbe.
Az sqlite állományokat felülírni, a többit dátumidő almappákba szervezve bemásolni,
a helyi állományokat pedig törölni sikeres adatátvitel után.*/

?>
