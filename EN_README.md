# BNPI Field Data Import ReadMe

## Short description

Custom module for OBM to import field data collected with QField, from BNPI cloud.

The module dwonloads the field data folder from cloud server (via public link), create a backup from sqlite files, convert sqlite point layers to csv and import it into an OBM project table.

After it delete imported elements from field database and load the empty (or partially empty) sqlite files, the backuped sqlite files back to the cloud.

## Requirements

Linux with, at least, the following installed packages:

- php
- gdal-bin (ogr2ogr, ogrinfo, gdalsrsinfo)
- unzip
- curl

## Syntax

You have to enter the following command in command line interface:

php dhte_field_data_import.php publikus-link

The public link for the data folder can be generated in the cloud after login.

Only the BNPI OwnCloud server supprted yet.
